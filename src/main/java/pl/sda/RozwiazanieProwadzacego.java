package pl.sda;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.errors.APIError;

import java.io.*;
import java.util.Locale;

public class RozwiazanieProwadzacego {
    public static void main(String[] args) throws APIError, IOException {
        DetectLanguage.apiKey = "6e4f850361c3d2eab677d3a794206ee8";

        File folder = new File("src/main/resources");
        File[] files = folder.listFiles();
        for (File file : files) {
            String wholeText = loadTextFromFile(file);
            String language = DetectLanguage.simpleDetect(wholeText);
            Locale locale = new Locale(language);

            System.out.println("Plik " + file.getName() + " jest w języku " + language + " (" + locale.getDisplayLanguage() + ").");
        }
    }

    private static String loadTextFromFile(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));

        String line;
        StringBuilder stringBuilder = new StringBuilder();
        while ((line = br.readLine()) != null) {
            stringBuilder.append(line).append(" ");
        }
        String wholeText = stringBuilder.toString();
        return wholeText;
    }
}