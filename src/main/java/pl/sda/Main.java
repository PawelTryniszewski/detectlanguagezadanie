package pl.sda;

import com.detectlanguage.DetectLanguage;
import com.detectlanguage.Result;
import com.detectlanguage.errors.APIError;

import java.io.*;
import java.util.List;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws APIError {
        DetectLanguage.apiKey = "6f6f3c6ce52797ce3a71f69eaa4a164c";
        DetectLanguage.ssl = true;

        String staraLinia = "";
        //StringBuilder duzy = new StringBuilder();
        File folder = new File("D:\\Projekty\\DetectLanguage\\src\\main\\resources");
        File[] lista = folder.listFiles();
        assert lista != null;
        String[] listaBuilderow = new String[lista.length];

        for (int i = 0; i < Objects.requireNonNull(folder.listFiles()).length; i++) {
            listaBuilderow[i] = "";

            try (BufferedReader reader = new BufferedReader(new FileReader(lista[i]))) {
                reader.readLine();
                while (reader.readLine() != null) {

                    String linia = reader.readLine();
                    if (!staraLinia.equals(linia)) {
                        listaBuilderow[i] += " " + linia;
                        // duzy.append(" "+linia);
                    }

                    Thread.sleep(100);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(listaBuilderow[i]);
            // String nowyDuzy = duzy.toString();
            List<Result> results = DetectLanguage.detect(listaBuilderow[i]);

            Result result = results.get(0);

            System.out.println("Language: " + result.language);
            System.out.println("Is reliable: " + result.isReliable);
            System.out.println("Confidence: " + result.confidence);
        }


    }
}
